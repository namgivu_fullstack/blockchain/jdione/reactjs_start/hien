import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import {
  HomePage,
  KeysPage,
  TransactionPage,
  SignaturesPage,
  BlockchainPage,
} from "./pages";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Link,
  NavLink,
  BrowserRouter,
} from "react-router-dom";
function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/keys" element={<KeysPage />} />
          <Route path="/signatures" element={<SignaturesPage />} />
          <Route path="/transaction" element={<TransactionPage />} />
          <Route path="/blockchain" element={<BlockchainPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
