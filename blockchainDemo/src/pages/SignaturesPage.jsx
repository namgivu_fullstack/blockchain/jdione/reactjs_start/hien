import React from "react";
import { Navbar,SignaturesComponent} from "../components";

const SignaturesPage = () => {
  return (
    <div className="flex flex-col">
      <Navbar />
      <SignaturesComponent />
    </div>
  );
};

export default SignaturesPage;
