import React from "react";
import { Navbar } from "../components";
const BlockchainPage = () => {
  return (
    <div className="flex flex-col">
      <Navbar />
      <div className="mt-[48px]">Blockchain Page</div>
    </div>
  );
};

export default BlockchainPage;
