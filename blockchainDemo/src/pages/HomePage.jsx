import React from "react";
import { Navbar } from "../components";

const HomePage = () => {
  return (
    <div>
      <Navbar />
      <div className="">HomePage</div>
    </div>
  );
};

export default HomePage;
