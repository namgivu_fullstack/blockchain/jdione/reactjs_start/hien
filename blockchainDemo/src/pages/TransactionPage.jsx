import React from "react";
import { Navbar } from "../components";
const TransactionPage = () => {
  return (
    <div className="flex flex-col">
      <Navbar />
      <div className="mt-[48px]">Transaction Page</div>
    </div>
  );
};

export default TransactionPage;
