import React from "react";
import { Navbar,KeysComponent } from "../components";
const KeysPage = () => {
  return (
    <div className="flex flex-col ">
      <Navbar />
      <KeysComponent />
    </div>
  );
};

export default KeysPage;
