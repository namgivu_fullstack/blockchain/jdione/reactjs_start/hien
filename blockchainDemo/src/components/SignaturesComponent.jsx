import React, { useState } from "react";
import { ec as EC } from "elliptic";
import * as secp256k1 from "secp256k1";
import { Buffer } from "buffer";
const ec = new EC("secp256k1");

const SignaturesComponent = () => {
  return (
    <div className="flex justify-center mt-[48px]">
      <div
        className=" w-[90%] border border-solid border-gray-300
                    rounded-lg text-left"
      >
        <div className="card-head bg-[#edf1f7] px-4 py-3 border-b border-gray-300">
          <h2 className="text-lg font-medium">Signatures</h2>
          
        </div>
        <div className="card-body p-4 space-y-4">
        <div>Message </div>
          <textarea className="w-full px-3 py-2 border border-gray-300 rounded-md "
          type ="text" rows={5}/>
          <div className="text-black text-base">Private Key</div>
          <div className="flex flex-col ">
            <input
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-blue-300"
              placeholder=""
            />
          </div>
          <button
            className="text-white w-full bg-[#007bff] hover:bg-[#0062cc]
          px-[0.375rem] py-[0.75rem] rounded-md text-base font-[400]"
          >
            Sign
          </button>
          <div>Message Signature</div>
          <input className="w-full px-3 py-2 border border-gray-300 rounded-md bg-gray-200"
          type ="text" disabled/>
        </div>
      </div>
    </div>
  );
};

export default SignaturesComponent;
