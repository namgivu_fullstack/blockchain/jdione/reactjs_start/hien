import React, { useState } from "react";
import { ec as EC } from "elliptic";
import * as secp256k1 from "secp256k1";
import { Buffer } from "buffer";

const ec = new EC("secp256k1");

const KeysComponent = () => {
  const [privateKey, setPrivateKey] = useState(generateRandomKey());
  const [publicKey, setPublicKey] = useState(getPublicKey(privateKey));

  function generateRandomKey() {
    const keyPair = ec.genKeyPair();
    const privateKey = keyPair.getPrivate("hex");
    return privateKey;
  }

  function getPublicKey(privateKey) {
    const privateKeyBytes = Buffer.from(privateKey, "hex");
    let publicKeyBytes = secp256k1.publicKeyCreate(privateKeyBytes, false);
    publicKeyBytes = publicKeyBytes.slice(1);
    const publicKeyHex = Array.from(publicKeyBytes, (byte) =>
      byte.toString(16).padStart(0, "0")
    ).join("");
    return publicKeyHex;
  }

  function handleRandomClick() {
    const newPrivateKey = generateRandomKey();
    setPrivateKey(newPrivateKey);
    setPublicKey(getPublicKey(newPrivateKey));
  }
  return (
    <div className="flex justify-center mt-[48px]">
      <div
        className=" w-[90%] border border-solid border-gray-300
                    rounded-lg text-left"
      >
        <div className="card-head bg-[#edf1f7] px-4 py-3 border">
          <h2 className="text-lg font-medium">Public / Private Key Pairs</h2>
        </div>
        <div className="card-body p-4 space-y-4">
          <div className="text-black text-base">Private Key</div>
          <div className="flex items-left ">
            <input
              className="w-full px-3 py-2 border border-gray-300 rounded-l-md focus:outline-none focus:ring focus:ring-blue-300"
              placeholder=""
              value={privateKey}
              onChange={(e) => setPrivateKey(e.target.value)}
            />
            <button
              className="px-3 py-2 bg-[#868e96] text-white rounded-r-md"
              onClick={handleRandomClick}
            >
              Random
            </button>
          </div>
          <div>Public Key</div>
          <input
            className="w-full px-3 py-2 border border-gray-300 rounded-md bg-gray-200"
            disabled
            value={publicKey}
          />
        </div>
      </div>
    </div>
  );
};

export default KeysComponent;
