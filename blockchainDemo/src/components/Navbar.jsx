import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  Route,
  Link,
  NavLink,
  Routes,
  useLocation,
} from "react-router-dom";

const Navbar = () => {
  const [currentLocation, setLocation] = useState(useLocation());
  return (
    <div
      className="border w-screen absolute top-0 left-0
    bg-[#343a40] text-center flex text-white 
    py-[12px] "
    >
      <div className="w-full text-left font-medium text-xl">
        Blockchain Demo: Public / Private Keys & Signing
      </div>
      <div className=" w-full text-right text-base flex flex-row justify-end gap-[12px]">
        <Link
          to="../keys"
          className={
            currentLocation.pathname == "/keys"
              ? "text-white"
              : "text-slate-400"
          }
        >
          Keys
        </Link>
        <Link
          to="../signatures"
          className={
            currentLocation.pathname == "/signatures"
              ? "text-white"
              : "text-slate-400"
          }
        >
          Signatures
        </Link>
        <Link
          to="../transaction"
          className={
            currentLocation.pathname == "/transaction"
              ? "text-white"
              : "text-slate-400"
          }
        >
          Transaction
        </Link>
        <Link
          to="../blockchain"
          className={
            currentLocation.pathname == "/blockchain"
              ? "text-white"
              : "text-slate-400"
          }
        >
          Blockchain
        </Link>
      </div>
    </div>
  );
};

export default Navbar;
